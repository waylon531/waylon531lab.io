---
title: "About"
date: 2019-03-06T18:33:13-08:00
---

Hello! I'm currently a Math student at Portland State, though I hope
to be graduating soon. I do a lot of hobbyist programming, I have both a 
[gitlab](https://gitlab.com/waylon531) and a [github](https://github.com/waylon531)
though most of my projects are located on gitlab. 
