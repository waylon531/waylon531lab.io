---
title: "Hello World!"
date: 2019-03-06T18:28:17-08:00
---

I've been meaning to set up a personal site for a while but haven't
gotten around to it, so welcome to my new page!
I'll try to start sticking stuff about what I'm working on 
programming-wise and some math stuff on here. Who knows
what else I'll use it for!

Welcome!
